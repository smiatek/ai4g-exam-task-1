import numpy as np
from task_1.config import config as config
from task_1.data_generator.random_data_generator import RandomDataGenerator
from task_1.simulator.simulation import Simulation

def run():
    np.random.seed(config["random_seed"])

    data_generator = RandomDataGenerator(config)
    sim = Simulation(data_generator, config)

    sim.simulate()

if __name__ == "__main__":
    run()



