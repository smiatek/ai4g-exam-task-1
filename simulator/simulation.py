from time import perf_counter_ns

import numpy as np
import os
import csv
import time

from task_1.background_writer import BackgroundWriter
from task_1.data_generator.data_generator import DataGenerator
from task_1.helpers.bg_writer_create_thread import bg_writer_create_thread
from task_1.helpers.custom_sleep import Sleep
from task_1.helpers.statistics import Statistics
from task_1.helpers.timeout import time_limit
from task_1.logger import Logger
from task_1.memory.game_state import GameState
from task_1.memory.memory_factory import MemoryFactory
from task_1.shared_data import SharedData


class Simulation:
    def __init__(self, data_generator : DataGenerator, config):
        self.data_generator = data_generator

        self.config = config
        self.timeout_for_handle_read = config["timeout_for_handle_read"]
        self.timeout_for_end_of_tick = config["timeout_for_end_of_tick"]
        self.timeout_for_handle_write = config["timeout_for_handle_write"]
        self.timeout_for_startup = config["timeout_for_startup"]
        self.expected_game_state_path = config["expected_game_state_path"]
        self.base_log_path = config["base_log_path"]

        self.current_tick_id = 0
        self.current_game_state = None
        self.game_state_size = config["game_state_size"]

        self.stats_for_logging = Statistics()

    def generate_data(self, config):
        initial_gs, updates = self.data_generator.generate_data(config)

        self.log_initial_game_state_and_update(initial_gs, updates)

        return initial_gs, updates

    def log_initial_game_state_and_update(self, initial_gs, updates):
        exp_gs_path = self.config["expected_game_state_path"]

        #save initial gs
        init_gs_path = os.path.join(exp_gs_path, "initial_gs.np")
        np.savetxt(init_gs_path, initial_gs)

        #save updates to gs
        with open(os.path.join(exp_gs_path, "updates.csv"), "w") as f:
            wr = csv.writer(f)
            wr.writerows(updates)


    def initialize_logger_and_bg_writer(self, initial_game_state) -> (Logger, BackgroundWriter):
        memory_factory = MemoryFactory(statistics=self.stats_for_logging, file_base_path=self.base_log_path)
        game_state = GameState(initial_game_state.copy(), self.stats_for_logging)

        shared_data = self.init_shared_data(memory_factory)
        logger = self.init_logger(game_state, shared_data)
        bg_writer = self.init_bg_writer(game_state, shared_data)

        return logger, bg_writer

    def init_shared_data(self, memory_factory):
        start_time_start_up = perf_counter_ns()

        with time_limit(self.timeout_for_startup):
            shared_data = SharedData(memory_factory)

        time_for_startup = perf_counter_ns() - start_time_start_up
        self.stats_for_logging.increase_start_up_time(time_for_startup)
        return shared_data

    def init_logger(self, game_state, shared_data):
        start_time_start_up = perf_counter_ns()

        with time_limit(self.timeout_for_startup):
            logger = Logger(game_state, shared_data, self.stats_for_logging)

        time_for_startup = perf_counter_ns() - start_time_start_up
        self.stats_for_logging.increase_start_up_time(time_for_startup)
        return logger

    def init_bg_writer(self, game_state, shared_data):
        sleeper = Sleep(self.stats_for_logging)

        start_time_start_up = perf_counter_ns()

        with time_limit(self.timeout_for_startup):
            bg_writer = BackgroundWriter(game_state=game_state, shared_data=shared_data, sleep=sleeper)

        time_for_startup = perf_counter_ns() - start_time_start_up
        self.stats_for_logging.increase_start_up_time(time_for_startup)
        return bg_writer

    def start_bg_writer_thread(self, bg_writer):
        t = bg_writer_create_thread(self.stats_for_logging, target=bg_writer.write_to_hard_disk)
        t.start()

        return t

    def start_up_logger(self, logger : Logger):
        # set timeout for startup
        start_time_start_up = perf_counter_ns()

        with time_limit(self.timeout_for_startup):
            logger.start_up()

        time_for_startup = perf_counter_ns() - start_time_start_up
        self.stats_for_logging.increase_start_up_time(time_for_startup)

    def perform_random_reads(self, logger, number_of_reads):
        # random reads
        for _ in range(number_of_reads):
            read_index = np.random.randint(0, self.game_state_size)
            self.perform_specific_read(logger, read_index)

    def perform_specific_read(self, logger, read_index):
        start_handle_read = perf_counter_ns()

        with time_limit(self.timeout_for_handle_read):
            value = logger.handle_read(read_index)

        delta = perf_counter_ns() - start_handle_read
        self.stats_for_logging.add_handle_read_time(self.current_tick_id, delta)
        if value != self.current_game_state[read_index]:
            raise Exception("Invalid game state read! Failed")

    def perform_end_of_tick(self, logger):
        start_time_tick = perf_counter_ns()

        with time_limit(self.timeout_for_end_of_tick):
            logger.end_of_tick(self.current_tick_id)

        time_for_tick = perf_counter_ns() - start_time_tick
        self.stats_for_logging.add_end_of_tick_time(self.current_tick_id, time_for_tick)

    def perform_write(self, index, logger, new_value):
        start_handle_write = perf_counter_ns()
        with time_limit(self.timeout_for_handle_write):
            logger.handle_write(index, new_value)

        delta = perf_counter_ns() - start_handle_write
        self.stats_for_logging.add_handle_write_time(self.current_tick_id, delta)
        self.current_game_state[index] = new_value

    def perform_shutdown(self, bg_writer_thread):
        #write last tick id to file
        with open(os.path.join(self.expected_game_state_path, "last_tick_id.txt"), "w") as f:
            f.write(f"{self.current_tick_id}\n")

        bg_writer_thread.kill() #kill bg writer thread
        bg_writer_thread.join(0.1) #in order to release resources


    def simulate(self):
        initial_game_state, timestamps_with_data = self.generate_data(self.config)
        self.current_game_state = initial_game_state.copy()
        logger, bg_writer = self.initialize_logger_and_bg_writer(initial_game_state)
        bg_writer_thread = self.start_bg_writer_thread(bg_writer)
        self.start_up_logger(logger)

        mean = []
        # game loop
        for changes in timestamps_with_data:
            print('changes per tick: '+ str(len(changes)))
            mean.append(len(changes))
            time.sleep(0.05)

            self.perform_random_reads(logger, 100)

            for index, new_value in changes:
                self.perform_write(index, logger, new_value)
                if np.random.rand() < 0.2:
                    self.perform_specific_read(logger, index)

                if np.random.rand() < 0.1:
                    self.perform_random_reads(logger, np.random.randint(1, 10))


            self.perform_end_of_tick(logger)

            if self.current_tick_id > 1000 and np.random.rand() < 0.01:
                self.perform_shutdown(bg_writer_thread)
                self.stats_for_logging.print_summary()
                return

            self.current_tick_id += 1
        mean = np.asarray(mean)
        print('mean:'+ str(mean))

        self.perform_shutdown(bg_writer_thread)

        self.stats_for_logging.print_summary()