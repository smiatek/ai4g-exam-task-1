from time import perf_counter_ns

import numpy as np
import os
import csv

from task_1.helpers.statistics import Statistics
from task_1.helpers.timeout import time_limit
from task_1.memory.game_state import GameState
from task_1.memory.memory_factory import MemoryFactory
from task_1.recover import Recover


class RecoveryCheck:
    def __init__(self, config):
        self.stats_for_recovery = Statistics()
        self.expected_game_state_path = config["expected_game_state_path"]
        self.timeout_for_recovery = config["timeout_for_recovery"]
        self.base_log_path = config["base_log_path"]

    def recover_gs_and_updates(self):
        #load initial gs
        init_gs_path = os.path.join(self.expected_game_state_path, "initial_gs.np")
        init_gs = np.loadtxt(init_gs_path)

        updates = []
        #save updates to gs
        with open(os.path.join(self.expected_game_state_path, "updates.csv"), "r") as f:
            cr = csv.reader(f)
            for i, line in enumerate(cr):
                updates.append(line)

        return init_gs, updates

    def find_checkpoint_in_ticks(self, recovered_gs : GameState, initial_gs, updates):
        recovered_array = recovered_gs.data
        current_gs = initial_gs
        current_tick = 0
        last_consistent_tick = -1

        if np.array_equal(recovered_array, current_gs):
            last_consistent_tick = 0

        for changes in updates:
            current_tick += 1

            for line in changes:
                split = line[1:len(line)-1].split(",")
                current_gs[int(split[0])] = float(split[1])

            if np.array_equal(recovered_array, current_gs):
                last_consistent_tick = current_tick

        if last_consistent_tick > 0:
            return last_consistent_tick
        else:
            raise Exception("Recovered game state is not consistent")

    def check_if_recovery_correct(self, given_gs : GameState):
        init_gs, updates = self.recover_gs_and_updates()
        tick_id = self.find_checkpoint_in_ticks(recovered_gs=given_gs, initial_gs=init_gs, updates=updates)
        return tick_id

    def do_recovery(self, stats_result_path):
        memory_factory = MemoryFactory(self.stats_for_recovery, self.base_log_path)
        recovery = Recover(memory_factory)

        start_recovery = perf_counter_ns()
        with time_limit(self.timeout_for_recovery):
            game_state = recovery.recover()

        delta = perf_counter_ns() - start_recovery

        self.stats_for_recovery.total_recovery_time = delta

        last_consistent_tick = self.check_if_recovery_correct(game_state)

        with open(os.path.join(self.expected_game_state_path, "last_tick_id.txt"), "r") as f:
            last_tick_in_simulation = int(f.readline())

        diff_recovery_last_tick = last_tick_in_simulation - last_consistent_tick
        self.stats_for_recovery.difference_between_last_tick_in_simulation_and_recover = diff_recovery_last_tick

        self.stats_for_recovery.last_recovered_consistent_tick = last_consistent_tick
        self.stats_for_recovery.write_all_stats(stats_result_path)
        self.stats_for_recovery.print_summary()