from time import sleep

from task_1.helpers.statistics import Statistics

#Do not modify
class Sleep:
    def __init__(self, stats : Statistics):
        self.stats = stats

    def sleep(self, secs):
        sleep(secs)
        self.stats.add_background_writer_time(-secs*1e9)