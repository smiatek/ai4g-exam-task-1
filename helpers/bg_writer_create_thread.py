import sys
import threading
from time import perf_counter_ns

from task_1.helpers.statistics import Statistics

#Do not modify
class bg_writer_create_thread(threading.Thread):
    def __init__(self, stats : Statistics , *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False
        self.stats = stats

    def start(self):
        self.__run_backup = self.run
        self.run = self.__run
        threading.Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        start = perf_counter_ns()
        try:
            self.__run_backup()
        finally:
            end = perf_counter_ns()
            delta = end - start
            self.stats.add_background_writer_time(delta)
            print(delta)
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        if event == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        if self.killed:
            if event == 'line':
                raise SystemExit()
        return self.localtrace

    def kill(self):
        self.killed = True