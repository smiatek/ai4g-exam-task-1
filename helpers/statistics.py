from time import perf_counter_ns
from collections import defaultdict
import json

#Do not modify
class Statistics:
    def __init__(self):
        self.start_up_time = 0.0
        self.number_of_checkpoints = -1
        self.last_time_checkpoint = 0
        self.checkpoint_durations = []

        self.handle_read_times = defaultdict(list)
        self.handle_write_times = defaultdict(list)
        self.end_of_tick_times = defaultdict(int)
        self.background_writer_time = 0.0

        #bit array ops
        self.bit_array_number_of_writes = 0
        self.bit_array_number_of_reads = 0
        self.bit_array_number_of_resets = 0
        self.number_of_bit_arrays = 0
        self.total_bit_array_size = 0

        #game states
        self.number_of_game_states = 0
        self.total_game_states_size = 0
        self.game_states_number_of_reads = 0
        self.game_states_number_of_writes = 0
        self.game_states_number_of_copys = 0

        #file operations
        self.file_ops_number_of_reads = 0
        self.file_ops_bytes_written = 0
        self.file_ops_bytes_read = 0
        self.file_ops_number_of_writes = 0
        self.file_ops_number_of_seeks = 0
        self.number_of_files = 0

        #total recovery time
        self.total_recovery_time = 0.0
        self.last_recovered_consistent_tick = None
        self.difference_between_last_tick_in_simulation_and_recover = None

    def add_handle_read_time(self, tick_id, time):
        self.handle_read_times[tick_id].append(time)

    def add_handle_write_time(self, tick_id, time):
        self.handle_write_times[tick_id].append(time)

    def add_end_of_tick_time(self, tick_id, time):
        self.end_of_tick_times[tick_id] += time

    def add_background_writer_time(self, time):
        self.background_writer_time += time

    def increase_start_up_time(self, increase):
        self.start_up_time += increase

    def add_checkpoint(self):
        self.number_of_checkpoints += 1
        if self.number_of_checkpoints > 0:
            end_time = perf_counter_ns()
            time_delta = end_time - self.last_time_checkpoint
            self.checkpoint_durations.append(time_delta)

        self.last_time_checkpoint = perf_counter_ns()

    def write_all_stats(self, path):
        all_vars = vars(self)
        with open(path, 'w') as f:
            json.dump(all_vars,f , default=repr)
            #print(all_vars, file=f)

    def print_summary(self):
        print("Summary:")
        print(f" Time Spent async BW writer (s) {self.background_writer_time / 1e+9}")
        print(f" Start Up Time (s): {self.start_up_time/1e+9}")

        if len(self.checkpoint_durations) > 0:
            print(f" Avg checkpoint time (s): {(sum(self.checkpoint_durations)/1e+9)/len(self.checkpoint_durations)}")

        print(f" Total checkpoint time (s): {(sum(self.checkpoint_durations) / 1e+9)}")




        all_vars = vars(self)

        for name, value in all_vars.items():
            print(f" {name} has value {value}")