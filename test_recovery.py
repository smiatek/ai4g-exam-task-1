import numpy as np
import os
from task_1.config import config
from task_1.simulator.recovery_check import RecoveryCheck

def run():
    np.random.seed(config["random_seed"])

    recovery_checker = RecoveryCheck(config=config)
    recovery_checker.do_recovery(os.path.join(config["base_log_path"], "recovery_results.txt"))

if __name__ == "__main__":
    run()
