import numpy as np

from task_1.memory.game_state import GameState
from task_1.shared_data import SharedData
from task_1.helpers.statistics import Statistics


class Logger:
    """
    This class should log and manage the game state
    """

    def __init__(self, game_state : GameState, shared_data : SharedData, stats : Statistics):
        self.stats = stats# do not modify
        self.game_state = game_state #reference to game state (do not modify)
        self.shared_data = shared_data #use this reference to share data with background writer (do not modify)

    def start_up(self):
        """
        Called when the start up is completed, the background writer thread is running.
        At this point in time there exists a valid game state! The size of the game state is already valid!
        Returns nothing
        -------

        """
        # TODO modify here
        #what if game crashes while first checkpoint?
        game_state_size = self.game_state.__len__()
        #initialize bg writer boolean variables
        self.shared_data.bg_writer_finished = True
        self.shared_data.start_bg_writer = False
        self.shared_data.current_update_no = 0
        #initialize file operators
        file_operator_game_state = self.shared_data.memory_factory.create_file_operator('fo_gamestate', 'game_state')
        #initialize current and previous application states
        self.shared_data.current_GS = np.zeros(game_state_size, dtype=(float))
        self.shared_data.previous_GS = self.game_state.data

        #initialize current and previous application state bit arrays
        self.shared_data.current_BA = self.shared_data.memory_factory.create_bit_array('current_BA', game_state_size)
        self.shared_data.previous_BA = self.shared_data.memory_factory.create_bit_array('previous_BA', game_state_size)
        # TODO efficiency
        #then reset them to 0 and 1 where necessary
        for i in range(game_state_size):
            if(self.shared_data.current_BA.__getitem__(i) == True):
                self.shared_data.current_BA.__setitem__( i, False)
        for i in range(game_state_size):
            if(self.shared_data.previous_BA.__getitem__(i) == True):
                self.shared_data.previous_BA.__setitem__( i, False)

        #save full copy of initial game state
        print('writing initial gs')
        file_operator_game_state.open()
        print('bytes written:' + str(file_operator_game_state.write(self.shared_data.previous_GS.tobytes())))
        file_operator_game_state.close()
        print('finished writing')

        pass

    def prepare_for_next_checkpoint(self):
        """
        Start a new checkpoint (time between to calls of this method is the checkpoint period).
        It must be ensured that a valid checkpoint can be recovered from the disk!
        Returns nothing
        -------

        """
        self.stats.add_checkpoint() #Do not modify this call or change its position
        # TODO modify here

        #As the previous values have been written to disk, previous_GS and previous_BA are repopulated
        #populate previous GS with values from current GS
        self.shared_data.previous_GS = self.shared_data.current_GS.copy()
        #populate previous bit array with values from current bit array
        for i in range(len(self.game_state)):
            self.shared_data.previous_BA.__setitem__(i, self.shared_data.current_BA.__getitem__(i))

        #values in current_GS and current_BA can now be reset
        #set current BA to zero
        self.shared_data.current_BA.reset_bit_array
        for i in range(len(self.game_state)):
            if(self.shared_data.current_BA.__getitem__(i) == True):
                self.shared_data.current_BA.__setitem__( i, False)
        #set current GS to zero
        self.shared_data.current_GS = np.zeros(self.game_state.__len__())

        #BG can be notified to start writing previous to disk
        self.shared_data.start_bg_writer = True
        pass

    def point_of_consistency(self):
        """
        Do stuff here like checking if the everything is written by the background writer and start a new checkpoint
        Returns nothing
        -------

        """
        # TODO modify here
        #check if there are new updates so as not to write superfluous nothing to file
        if self.shared_data.bg_writer_finished == True:
            if self.shared_data.new_updates == True:
                self.shared_data.new_updates = False
                print('logger_end of checkpoint')
                self.prepare_for_next_checkpoint()

        pass

    def handle_read(self, index) -> np.float:
        """
        Need to handle reads to the game state
        Parameters
        ----------
        index Which index of the game state should be read

        Returns The value of the game state
        -------

        """
        # TODO modify here

        return self.game_state[index]#example code

    def handle_write(self, index, new_value):
        """
        Need to handle updates to the game state
        Parameters
        ----------
        index Index of the changed game state
        new_value The new value of the game state

        Returns nothing
        -------

        """

        # TODO modify here
        self.shared_data.new_updates = True
        #update value in game state
        self.game_state[index] = new_value
        #update value in current AS
        self.shared_data.current_GS[index] = new_value
        #set dirty bit
        self.shared_data.current_BA.__setitem__(index, True)

    def end_of_tick(self, tick_id):
        """
        Indicates the end of the tick and that there will be no more writes to the game state in the current tick
        Parameters
        ----------
        tick_id current tick id

        Returns nothing
        -------

        """
        # TODO modify here
        print('logger_end of tick')
        self.point_of_consistency()#example code