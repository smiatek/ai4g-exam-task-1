import numpy as np

from task_1.memory.memory import Memory

#Do not modify
class BitArray(Memory):
    """
    This class represents a bit array
    """
    def __init__(self, size, statistics):
        super().__init__(statistics)
        self.stats.total_bit_array_size += size
        self.stats.number_of_bit_arrays += 1
        self.storage = np.empty(size, dtype=np.bool_)

    def read_bit(self, position):
        """
        Read a single bit at the given position
        Parameters
        ----------
        position Represents the position to read

        Returns The value of the bit at position
        -------

        """
        self.stats.bit_array_number_of_reads += 1
        return self.storage[position]

    def write_bit(self, position : int, value : np.bool_):
        """
        Write a bit at the given position
        Parameters
        ----------
        position Position to write
        value Which value to write at the given position

        Returns Nothing
        -------

        """
        self.storage[position] = value
        self.stats.bit_array_number_of_writes += 1

    def reset_bit_array(self):
        """
        Resets the bit array
        Returns Nothing
        -------

        """
        self.storage = np.empty(len(self.storage), dtype=np.bool_)
        self.stats.bit_array_number_of_resets += 1

    def __getitem__(self, item):
        return self.read_bit(item)

    def __setitem__(self, key, value):
        self.write_bit(key, value)
