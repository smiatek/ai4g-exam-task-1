from io import RawIOBase
from typing import Union, Optional
from time import sleep
from task_1.memory.memory import Memory
from task_1.helpers.statistics import Statistics

#Do not modify
class FileOperator(Memory):
    """
    This class provides methods to perform file operations
    """

    def __init__(self, file_name: Union[str, int, bytes], statistics: Statistics):
        """
        Parameters
        ----------
        file_name Name of the file
        statistics Statistics for measurements
        """
        super().__init__(statistics)
        self.file_name = file_name
        self.file : RawIOBase = None

    def open(self):
        """
        Open the file in mode ab+ with no buffering
        Returns Nothing
        -------

        """
        self.stats.number_of_files += 1
        self.file = open(self.file_name, "ab+", buffering=0)

    def seek(self, offset, whence=0):
        """
        Seek to a specific position. Compare to io.IOBase.seek
        Parameters
        ----------
        offset
        whence

        Returns
        -------

        """
        self.stats.file_ops_number_of_seeks += 1
        return self.file.seek(offset, whence)

    def write(self, b : Union[bytes, bytearray]) -> Optional[int]:
        """
        Write bytes to file.
        Parameters
        ----------
        b The bytes to write.

        Returns number of bytes written.
        -------

        """
        self.stats.file_ops_number_of_writes += 1
        bytes_written = self.file.write(b)
        if bytes_written is not None:
            self.stats.file_ops_bytes_written += bytes_written
            sleep(0.0001*bytes_written)
        return bytes_written


    def read(self, size : int =-1) -> Optional[bytes]:
        """
        Read from file.
        Parameters
        ----------
        size

        Returns
        -------

        """
        self.stats.file_ops_number_of_reads += 1
        read =  self.file.read(size)
        if read is not None:
            self.stats.file_ops_number_of_reads += len(read)
        return read

    def tell(self) -> int:
        """
        Get current position in file
        Returns current position in file
        -------

        """
        return self.file.tell()

    def flush(self):
        """
        Flush buffers
        Returns nothing
        -------

        """
        self.file.flush()

    def close(self):
        """
        Close the file.
        Returns nothing
        -------

        """
        self.file.close()