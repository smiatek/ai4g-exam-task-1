from typing import Union
import os
import numpy as np

from task_1.memory.bit_array import BitArray
from task_1.memory.file_operator import FileOperator
from task_1.memory.game_state import GameState

#Do not modify
class MemoryFactory:
    def __init__(self, statistics, file_base_path):
        self.stats = statistics
        self.memories = {}
        self.file_base_path = file_base_path

    def get_by_id(self, ident : str) -> Union[GameState, BitArray, FileOperator]:
        """
        Access a memory by its identifier.
        Parameters
        ----------
        ident identifier

        Returns requested memory
        -------

        """
        return self.memories[ident]

    def create_game_state(self, ident : str, size : int) -> GameState:
        """
        Creates a new game state.
        Parameters
        ----------
        ident identifier
        size size of the game state

        Returns Game state.
        -------

        """
        game_state =  GameState(np.random.rand(size), self.stats)
        self.memories[ident] = game_state
        return game_state

    def create_copy_of_game_sate(self, ident : str, game_state_to_copy : GameState) -> GameState:
        """
        Create a copy of a game state
        Parameters
        ----------
        ident identifier of the new game state
        game_state_to_copy game state to copy

        Returns new copy of the game state
        -------

        """
        new_game_state = GameState(game_state_to_copy.data.copy(), self.stats)
        self.memories[ident] = new_game_state
        self.stats.game_states_number_of_copys += 1
        return new_game_state


    def create_bit_array(self, ident : str, size : int) -> BitArray:
        """
        Create a bit array.
        Parameters
        ----------
        ident identifier
        size size of the bit array

        Returns bit array
        -------

        """
        bit_array =  BitArray(size, self.stats)
        self.memories[ident] = bit_array
        return bit_array

    def create_file_operator(self, ident : str, file_name : str) -> FileOperator:
        """
        Create a file operator.
        Parameters
        ----------
        ident identifier
        file_name file name

        Returns file operator
        -------

        """
        file_path = os.path.join(self.file_base_path, file_name)
        file_operator = FileOperator(file_path, self.stats)
        self.memories[ident] = file_operator
        return file_operator
