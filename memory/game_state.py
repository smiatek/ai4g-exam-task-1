from task_1.memory.memory import Memory
from task_1.helpers.statistics import Statistics

#Do not modify
class GameState(Memory):
    """
    This class represents the game state
    """
    def __init__(self, array, statistics: Statistics):
        super().__init__(statistics)
        self.stats.number_of_game_states += 1
        self.stats.total_game_states_size += len(array)
        self.data = array #you can access this field, e.g. in order create shadow with numpy

    def __getitem__(self, key):
        self.stats.game_states_number_of_reads += 1
        return self.data[key]

    def __setitem__(self, key, value):
        self.stats.game_states_number_of_writes += 1
        self.data[key] = value


    def __len__(self):
        return len(self.data)