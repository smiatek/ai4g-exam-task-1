from task_1.memory.game_state import GameState
from task_1.memory.memory_factory import MemoryFactory
import numpy as np


class Recover:
    #do not modify this method
    def __init__(self,  memory_factory : MemoryFactory):
        self.memory_factory = memory_factory

    def recover(self) -> GameState:
        """
        You must be able to recreate a valid game state from files

        Returns The recovered game state
        -------

        """
        #open and read intial gamestate from file
        file_operator_gs = self.memory_factory.create_file_operator('fo_gamestate', 'game_state')
        file_operator_gs.open()
        # read values from file
        file_operator_gs.seek(0, 0)
        byteFile = file_operator_gs.read(-1)
        recovered_GS_data = np.frombuffer(byteFile, dtype="float64").copy()
        file_operator_gs.close()

       #just loop over update files as long as they have content (=because they exist)
        i=0
        while True:
            i=i+1#current update number

            file_operator_v = self.memory_factory.create_file_operator('update_v_' + str(i),
                                                                       'update_v_' + str(i))
            file_operator_v.open()
            # read values from file
            file_operator_v.seek(0, 0)
            byteFile = file_operator_v.read(-1)
            values_array = np.frombuffer(byteFile, dtype="float64")
            #file_operator_v.flush()
            file_operator_v.close()

            # create and open file operator to write updated values' indexes to file
            file_operator_i = self.memory_factory.create_file_operator(
                'update_i_' + str(i), 'update_i_' + str(i))
            file_operator_i.open()
            file_operator_i.seek(0, 0)
            byteFile = file_operator_i.read(-1)
            indexes_array = np.frombuffer(byteFile, dtype="float64").astype('int')
            file_operator_i.close()

            #check if both files exist and have been fully written
            if len(indexes_array)== 0 or len(values_array)== 0:
                #either only one of the files was written or the end of update files has been reached
                #in any case:
                break
            #sanity check that both arrays have the same size
            elif len(indexes_array) == len(values_array):
                recovered_GS_data[indexes_array] =  values_array
                print(i)

        #all update files have been read and applied to the initial gamestate, it's result is now
        #ready to be set as the recovered gamestate
        recovered_GS = self.memory_factory.create_game_state('recovered_GS', recovered_GS_data.__len__())
        recovered_GS.data = recovered_GS_data

        return recovered_GS