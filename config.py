config = {
    "game_state_size" : 20000, #number of variables in the game state
    "timeout_for_handle_read" : 5.0, #timeout for a single handle read call in seconds
    "timeout_for_end_of_tick" : 20.0, #timeout for a single end of tick call in seconds
    "timeout_for_handle_write" : 5.0, #timeout for a single handle write call in seconds
    "timeout_for_startup" : 30.0, #timeout for each startup operation in seconds
    "timeout_for_recovery" : 60.0, #timeout for the recovery operation in seconds
    "expected_game_state_path" : "/tmp/expected_game_state", #path where the simulation stores the expected game state (necessary for recovery check)
    "number_of_ticks" : 1000, #how many ticks should be simulated
    "random_seed": 1,  # numpy random seed
    "base_log_path": "/tmp/base_log_path",  # base path to for log files to write/read
}

configDebug = {
    "game_state_size" : 20000, #number of variables in the game state
    "timeout_for_handle_read" : 5.0, #timeout for a single handle read call in seconds
    "timeout_for_end_of_tick" : 3294902380293.0, #timeout for a single end of tick call in seconds
    "timeout_for_handle_write" : 5.0, #timeout for a single handle write call in seconds
    "timeout_for_startup" : 3049580349.0, #timeout for each startup operation in seconds
    "timeout_for_recovery" : 60.0, #timeout for the recovery operation in seconds
    "expected_game_state_path" : "/tmp/expected_game_state", #path where the simulation stores the expected game state (necessary for recovery check)
    "number_of_ticks" : 1000, #how many ticks should be simulated
    "random_seed": 1,  # numpy random seed
    "base_log_path": "/tmp/base_log_path",  # base path to for log files to write/read
}