from abc import abstractmethod, ABC

#Do not modify
from numpy import ndarray


class DataGenerator(ABC):
    """
    Generate modifications to game state
    """
    def __init__(self, game_state_size):
        self.game_state_size = game_state_size

    @abstractmethod
    def generate_data(self, config) -> (ndarray, [list]):
        pass
