import numpy as np

from task_1.data_generator.data_generator import DataGenerator

class RandomDataGenerator(DataGenerator):
    """
    Class that generates random modifications to the game state
    """
    def __init__(self, config):
        super().__init__(config["game_state_size"])
        self.time_interval = config["number_of_ticks"] #number of ticks

    def generate_data(self, config):
        initial_game_state = np.random.rand(self.game_state_size)

        timestamps_with_data = [[] for _ in range(self.time_interval)]

        for game_state_index in range(self.game_state_size):
            timestamp = 0.0

            while True:  # generate until max time interval
                time_delta = np.random.randint(1, 100)
                timestamp += time_delta
                timestamp_index = int(timestamp)

                if timestamp_index >= self.time_interval:
                    break


                new_game_state_value = np.random.rand()

                timestamps_with_data[timestamp_index].append((game_state_index, new_game_state_value))

        return initial_game_state, timestamps_with_data
