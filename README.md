# Artificial Intelligence For Games Task 1
## Your Task
In this assignment, your task is to implement a fast checkpoint recovery algorithm for a simulated game.
The game may crash at an unknown time. You then must be able to recover a consistent games state from the last checkpoint.
Therefore we expect you to manage the game state, create logs of the game state and recover the game state from the log files.

Choosing a suitable checkpoint recovery algorithm is important and depends on the simulated games.
As in many real world-games in our simulation, only a small part of the game state is updated in each tick.

Additionally, we ask you to provide us with a short explanation (3-5 sentences) on why you have chosen the algorithm that you have implemented.
## Requirements
If you do not comply with the requirements, you will fail the task.
### Timeouts
There are several timeouts defined. If you do not comply with these timeouts you will fail the task.

##### Startup Timeout
Timeout for each startup operation. Startup operations are the `start_up()` call and 
the constructor calls of `Logger`, `BackgroundWriter` and `SharedData`.

##### Handle Read Timeout
Timeout for a single `handle_read()` call.

##### Handle Write Timeout
Timeout for a single `handle_write()` call.

##### Handle End Of Tick Timeout
Timeout for a single `end_of_tick()` call.

##### Recovery Timeout
Timeout for the `recover()` method.

### Structure of your solution
For your solution we expect you to upload the following files to Moodle:
* `background_writer.py`
* `shared_data.py`
* `logger.py`
* `recover.py`
* `description.txt`

We expect the explanation on why you have chosen the algorithm implemented in `description.txt`.
### File Modifications
 You are not allowed to make modifications to any file except stated otherwise.
 You should not rename any file or create additional files.
 You are allowed to make changes to the following files (there may be instructions on how to exactly modify these files in the files):
* `background_writer.py`
* `logger.py`
* `recover.py`
* `shared_data.py`

However, you are not allowed to change the name of the classes defined in these files, remove imports or the signature of methods in these files.

There are files provided to allow you to test your solution:
* `test_logging.py`: Runs a simulation of the logging
* `test_recovery.py`: Runs a recovery of previously logged data
* `simulation.py`: Simulates a game
* `recovery_check.py`: Simulates the recovery of the game state
* `config.py`: Configuration for simulation/recovery
* `random_data_generator.py`: Generates changes for the simulation of the game


### Accessing Files
 You are only allowed to perform IO through the `FileOperation` class. 
 It is not permitted to perform file operations through any other means.

### Allowed Imports
 You are only allowed to use the python standard library and numpy. Please keep in mind to only perform IO operations 
 through the `FileOperation` class. Removing imports or replacing imports at runtime is not permitted.

### Illegal Behavior
 * You are not allowed to interfere with the statistics class. Any attempt to do so will be considered as cheating.
 * If your program crashes or runs into timeouts, you will not pass this task.
 * You must create checkpoints in your logger and be able to recover a consistent game state.
 * You are not allowed to access/modify our simulation or prevent/circumvent our evaluation (e.g. manipulate timeouts, access future changes to game state, ...).
 * You must create the solution solely by yourself. We will use software and other measures to detect plagiarism. We have already detected plagiarism in exercise.
  
## Overview of the structure
The simulation creates updates to the game state and also has to read from the game state. 
The Logger must manage the game state and create checkpoints.
The simulation notifies the logger about changes to the game state through `handle_write`. The end of the tick, which 
means that there will be no more changes to game state in this tick, is indicated to the logger through `end_of_tick`.

The BackgroundWriter runs in the background and allows you to asynchronously perform operations in the background.
The Logger can communicate and share data with BackgroundWriter through SharedData.

The MemoryFactory, which is present in SharedData, allows the creation of Memories. 

Memories are useful data structures and allow file operations.

To better understand the interactions we also attached a class diagram (class_diagram.pdf).
#### shared_data.py
 In this class, you are allowed to modify the constructor in order to create class variables.
 You can use it to share data between the logger and the background_writer.
 
#### logger.py
 This class needs to manage the game state and be able to handle reads and writes to game state and log theses changes.

##### `start_up()`
 Called upon start up.

##### `end_of_tick()`
 Indicates the end of the tick and that there will be no more writes to the game state in the current tick.

##### `prepare_for_next_checkpoint()`
 You must call this method to indicate that you have written a valid checkpoint to the disk. 
 The time between to calls of this method is referred as the checkpoint period.
 
##### `handle_read()`
 Handle a read to the game state.

##### `handle_write()`
 Handle an update to the game state.


#### background_writer.py 
 You can use it to write changes in the background to the hard disk.
##### `write_to_hard_disk()`
 This method runs in the background. Its running a loop to perform work.
 As we assume a system with just a single core and due to limitations of pythons, performing CPU-bound work in this thread
 will also affect the whole systems performance.

#### recover.py
 Use this class to recover the game state.

#### Memory
##### Bit Array
This data structure is an array of bits. It is useful for a number of algorithms.
##### File Operator
This class provides a way to perform file operations on a single file.
An instance of FileOperator is bound to a single file. If the file does not exist it will be created by the open operation.
You can read bytes from the file or write bytes to the file.

 `open()`
 
 Opens the file in mode ab+ with buffering disabled.
 
 `close()`
 
 Closes the file.
 
 `seek()`
 
 See: https://docs.python.org/3/library/io.html#io.IOBase.seek
 
 `tell()`
 
 See: https://docs.python.org/3/library/io.html#io.IOBase.tell
 
 `read()`
 
 See: https://docs.python.org/3/library/io.html#io.RawIOBase.read
 
 `write()`
 
 See: https://docs.python.org/3/library/io.html#io.RawIOBase.write
##### Game State
 This class is a representation of a game state. The game state is represented as a number of atoms. 
 Each atom has a fixed position _i_ in the game state.
 You can use the bracket syntax to read or write an atom in the game state.

##### Memory Factory
Memory Factory is the only permitted way to create instances of memories. It is present in shared data and recovery.

`get_by_id()`

Get a memory by its identifier.


`create_game_state()`

Create a new game state.


`create_copy_of_game_sate()`

Create a new game state based on existing one.


`create_bit_array()`

Create a bit array.

`create_file_operator()`

Create a file operator.

## Evaluation

Passing the checks in the provided VM gives you an indication of how our test will work. 
However, your code must pass the checks on our machines (using the VM provided to you) and we will test your solution in various different scenarios.

You can achieve up to 11 points in this task.

The following points must be fulfilled by your solution in order to be able to get any points for this task. 
The correctness of your solution must be guaranteed during any test we will run. 

* Correctness of your solution (4 points)
  * No exceptions/crashes
  * Recovered game state consistent
  * Correct reads (handle_read) and updates (handle_write) to game state during any time
* Explanation on why you have chosen the algorithm implemented (1 point)

You can achieve additional points which improve your grading from 4.0 towards 1.0 through the following criteria:

* Number of checkpoints created [higher number is better] (3 points)
* Lag created per tick (lag consists of times for handle_read, handle_write and end_of_tick) [smaller is better] (2 points)
* Size of your log files [smaller is better] (1 point)
## Virtual Machine For Testing
In order to be able to have a similar test environment, we provide a virtual machine.
You can download the VM in the Open Virtualization Format under: https://www.dbs.ifi.lmu.de/~strauss/ai4g/ai4g_task_1_vm.zip (username:ai4g, password:QHxEm2sEkdcv). In order to use this VM you can use for example the free software VirtualBox.

Credentials:

Username: ai4g

Password: ai4g

In order to run checks place the following files in `/home/ai4g/submission`:

* `background_writer.py`
* `logger.py`
* `recover.py`
* `shared_data.py`

After that you can execute the checks on the Terminal of the VM using the following command:

`mkdir -p /tmp/expected_gs_path && python3.8 /home/ai4g/task_checker_students/test_student_submission.py`
## FAQ
### Writing/Reading Files
Use `FileOperator` class. When opening a file, the position of the file stream is placed at the end of the file.
If a file does not exist it will be created.
### How To Debug
Before running create temporary directories for expected_game_state_path and
base_log_path. Add them to `config.py`. 
Then `execute test_logging.py` or `test_recovery.py`. 
Make sure you add the path containing task_1 to the python path and set task_1 as the working directory.

E.g. running in python console:

    import sys
    sys.path.extend(['/path_to_folder_containing_task_1'])
    import task_1.test_logging
    task_1.test_logging.run()
