from task_1.memory.memory_factory import MemoryFactory

class SharedData():
    def __init__(self, memory_factory : MemoryFactory):
        self.memory_factory = memory_factory
        #You are allowed to define primitive types here: int, bool and references to memories
        # TODO modify here
        self.current_GS = None
        self.previous_GS = None

        self.current_BA = None
        self.previous_BA  = None

        self.new_updates = None

        self.bg_writer_finished = None
        self.start_bg_writer = None
        self.current_update_no = None

        self.file_operator_game_state = None


