from task_1.helpers.custom_sleep import Sleep
from task_1.memory.game_state import GameState
from task_1.shared_data import SharedData

#TODO probably not  allowed
import numpy as np

class BackgroundWriter:
    """
    This class can perform operations in background
    """
    def __init__(self, game_state : GameState, shared_data : SharedData, sleep : Sleep):
        self.game_state = game_state
        self.shared_data = shared_data
        self.sleep = sleep

    def write_to_hard_disk(self):
        """
        This method is called in background. Time sleeping (if you use self.sleep.sleep()) is not counted towards async bg time.

        Returns nothing
        -------

        """



        while True:#do not modify
            # TODO modify here
            if self.shared_data.start_bg_writer == True:

                #increment current update number  and save it
                self.shared_data.current_update_no += 1
                current_update_no = self.shared_data.current_update_no

                #notify logger that bg_writer has started and is currently occupied
                self.shared_data.start_bg_writer = False
                self.shared_data.bg_writer_finished = False

                #TODO maybe check if there ARE any updates (like for the first update that is zero)
                #build simple arrays of updated values and updated value's indexes (positions) to save to file
                value_array = []
                position_array = []
                #iterate over all values in the current gamestate when the writer thread was called
                for value_pos in range(len(self.shared_data.previous_GS)):
                    self.sleep.sleep(
                        0.00001)  # for some reason this is needed in order to give the loop time to access the variables

                    #this is the value of the bit_array at position value_pos (TRUE when the valued at this position has been updated)
                    item = self.shared_data.previous_BA.read_bit(value_pos)
                    # create actual BA since we cannot access .storage
                    position_array.append(item)
                    if item:
                        #create array of only updated values
                        value_array.append(self.shared_data.previous_GS.data[value_pos])
                value_array = np.asarray(value_array)
                position_array = np.where(position_array)[
                    0].astype('float64')#where item is true

                #check if updates are empty, else continue
                if len(value_array)>0:

                    #create and open file operator to write value updates to file
                    file_operator_v = self.shared_data.memory_factory.create_file_operator('update_v_'+str(current_update_no), 'update_v_'+str(current_update_no))
                    file_operator_v.open()
                    #write value updates to file
                    print('UPDATE' + str(current_update_no) + 'V bytes written:' + str(file_operator_v.write(value_array.tobytes())))
                    file_operator_v.close()

                    #create and open file operator to write updated values' indexes to file
                    file_operator_i = self.shared_data.memory_factory.create_file_operator(
                        'update_i_' + str(current_update_no), 'update_i_' + str(current_update_no))
                    file_operator_i.open()
                    # write value updates to file
                    print('UPDATE' + str(current_update_no) + 'I bytes written:' + str(file_operator_i.write(position_array.tobytes())))
                    file_operator_i.close()

                #notify logger that writer thread is finished
                self.shared_data.bg_writer_finished = True

            self.sleep.sleep(0.05)#do not modify